<?php

namespace App\Http\Requests;

use App\Exceptions\ValidationResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BaseRequest extends FormRequest
{
    /**
     * This will override the Request validator response
     *
     * @param Validator $validator
     * @throws ValidationResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationResponseException('Invalid form values!', $validator->errors());
    }

    public function messages()
    {
        return [

        ];
    }
}
