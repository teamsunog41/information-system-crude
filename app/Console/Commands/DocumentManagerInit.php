<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class DocumentManagerInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'document-manager:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize document manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(PHP_EOL . 'Initializing document manager...' . PHP_EOL);

        // Create symlink
        $this->call('storage:link');

        // Generate default directories
        Storage::disk('public')->makeDirectory('SI/Documents');
        Storage::disk('public')->makeDirectory('SI/Pictures');
    }
}
