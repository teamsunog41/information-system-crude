<?php

/*
|--------------------------------------------------------------------------
| Test Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "api" middleware group. Now create something great!
|
*/

// Room Routes
Route::group(['prefix' => 'rooms'], function () {
    // Browse Rooms
    Route::get('', [
        'uses' => 'RoomController@browse',
    ]);
    // Read Rooms
    Route::get('/{id}', [
        'uses' => 'RoomController@read',
    ]);
    // Edit Rooms
    Route::put('/{id}', [
        'uses' => 'RoomController@edit',
    ]);
    // Add Rooms
    Route::post('/', [
        'uses' => 'RoomController@add',
    ]);
    // Delete Rooms
    Route::delete('/{id}', [
        'uses' => 'RoomController@delete',
    ]);
});

// Event Routes
Route::group(['prefix' => 'events'], function () {
    // Browse Events
    Route::get('', [
        'uses' => 'EventController@browse',
    ]);
    // Read Events
    Route::get('/{id}', [
        'uses' => 'EventController@read',
    ]);
    // Edit Events
    Route::put('/{id}', [
        'uses' => 'EventController@edit',
    ]);
    // Add Events
    Route::post('/', [
        'uses' => 'EventController@add',
    ]);
    // Delete Events
    Route::delete('/{id}', [
        'uses' => 'EventController@delete',
    ]);
});

// Schedule Routes
Route::group(['prefix' => 'schedules'], function () {
    // Browse Schedules
    Route::get('', [
        'uses' => 'ScheduleController@browse',
    ]);
    // Read Schedules
    Route::get('/{id}', [
        'uses' => 'ScheduleController@read',
    ]);
    // Edit Schedules
    Route::put('/{id}', [
        'uses' => 'ScheduleController@edit',
    ]);
    // Add Schedules
    Route::post('/', [
        'uses' => 'ScheduleController@add',
    ]);
    // Delete Schedules
    Route::delete('/{id}', [
        'uses' => 'ScheduleController@delete',
    ]);
});
