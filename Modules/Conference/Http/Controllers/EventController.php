<?php

namespace Modules\Conference\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Conference\Entities\Event;

class EventController extends Controller
{
    /**
     * Browse a listing of the resource.
     * @return Response
     */
    public function browse()
    {
        $events = Event::all();

        return $events;
    }

    /**
     * Read a specified resource.
     * @param int $id
     * @return Response
     */
    public function read($id)
    {
        $event = Event::find($id);

        return $event;
    }

    /**
     * Edit a specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->update($request->all());

        return response()->json($event, 200);
    }

    /**
     * Add a new resource.
     * @return Response
     */
    public function add(Request $request)
    {
        $event = Event::create($request->all());

        return response()->json($event, 201);
    }

    /**
     * Delete a specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        return response()->json(null, 204);
    }
}
