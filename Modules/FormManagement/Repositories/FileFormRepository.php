<?php

namespace Modules\FormManagement\Repositories;

use App\Repositories\Repository;
use Modules\FormManagement\Entities\FileForms;
use Modules\FormManagement\Exceptions\FormRequestException;

class FileFormRepository extends Repository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed
     */
    public function model()
    {
        return FileForms::class;
    }

    /**
     * @param $leaveFormData
     * @return string
     * @throws FormRequestException
     */
    private function validateArrayFieldForLeaveForm($leaveFormData)
    {
        //validate leave form field required
        if (!isset($leaveFormData['reasons_of_absence'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields reasons of absence']
            );
        } elseif (!isset($leaveFormData['inclusive_dates_from'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields inclusive dates from']
            );
        } elseif (!isset($leaveFormData['inclusive_dates_to'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields inclusive dates to']
            );
        } elseif (!isset($leaveFormData['number_of_days_or_hours'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields number of Days/Hours']
            );
        }
    }

    /**
     * @param $changeShiftFormData
     * @return string
     * @throws FormRequestException
     */
    private function validateArrayFieldForChangeShiftForm($changeShiftFormData)
    {
        //validate Change shift form required
        if (!isset($changeShiftFormData['current_work_schedule'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields current work schedule']
            );
        } elseif (!isset($changeShiftFormData['new_work_schedule'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields new work schedule']
            );
        } elseif (!isset($changeShiftFormData['reasons_or_justification'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields reasons or justification']
            );
        }
    }

    /**
     * @param $underTimeFormData
     * @return string
     * @throws FormRequestException
     */
    private function validateArrayFieldForUndertimeForm($underTimeFormData)
    {
        //validate undertime form field required
        if (!isset($underTimeFormData['date_of_overtime'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields date of overtime']
            );
        } elseif (!isset($underTimeFormData['time_start'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields time start']
            );
        } elseif (!isset($underTimeFormData['time_finished'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields time finished']
            );
        } elseif (!isset($underTimeFormData['total_no'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields total no of hours']
            );
        } elseif (!isset($underTimeFormData['purpose'])) {
            throw new FormRequestException(
                'Validation error!',
                ['Required fields purpose']
            );
        }
    }

    /**
     * @return mixed
     */
    public function getAllFileRequestForm()
    {
        return [
            'message' => 'List of all request form.',
            'data' => $this->all(),
        ];
    }

    /**
     * @param $request
     */
    public function addFileRequestForm($request)
    {
        if ($request->type_of_request == config('siis.request_type.leave_form')) {
            $leaveFormData = $request->unique_column;
            $leaveValidate = $this->validateArrayFieldForLeaveForm($leaveFormData);
        } elseif ($request->type_of_request == config('siis.request_type.change_shift_form')) {
            $changeShiftFormData = $request->unique_column;
            $changeShiftValidate = $this->validateArrayFieldForChangeShiftForm($changeShiftFormData);
        } else {
            $underTimeFormData = $request->unique_column;
            $underTimeValidate = $this->validateArrayFieldForUndertimeForm($underTimeFormData);
        }

        try {
            if ($request->has('type_of_request')) {
                $this->createNew([
                    'employee_name' => $request->employee_name,
                    'employee_number' => $request->employee_number,
                    'position' => $request->position,
                    'department' => $request->department,
                    'date_hired' => $request->date_hired,
                    'approved_by' => $request->approved_by,
                    'checked_by' => $request->checked_by,
                    'type_of_request' => $request->type_of_request,
                    'status' => FileForms::REQUEST_FORM_STATUS,
                    'request_information' => json_encode($request->unique_column),
                ]);
            }
            return response()->json(['message' => 'File request form successfully added.'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to create file request form.'], 500);
        }
    }

    /**
     * @param $request
     */
    public function updateFileRequestForm($request)
    {
        if (!$formRequest = $this->find($request->id)) {
            return response()->json(['message' => 'Validation error!', 'error' => 'Record not found!'], 422);
        }

        try {
            $params = [
                'employee_name' => $request->employee_name,
                'employee_number' => $request->employee_number,
                'position' => $request->position,
                'department' => $request->department,
                'date_hired' => $request->date_hired,
                'approved_by' => $request->approved_by,
                'status' => $request->status,
                'checked_by' => $request->checked_by,
                'type_of_request' => $request->type_of_request,
            ];

            $this->update(
                $request->id,
                $params
            );

            return response()->json(['message' => 'File request form successfully updated.'], 201);
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Failed to update request form.'], 500);
        }
    }

    /**
     * @param $id
     */
    public function deleteFileRequestForm($id)
    {
        if (!$formRequest = $this->find($id)) {
            return response()->json(['message' => 'Validation error!', 'error' => 'Record not found!'], 422);
        }
        try {
            $formRequest->delete();
            return response()->json(['message' => 'Request form successfully deleted.'], 201);
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Failed to delete request form.'], 500);
        }
    }
}
