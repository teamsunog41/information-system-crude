<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileRequestFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_request_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_name', 50)->nullable();
            $table->string('employee_number')->nullable();
            $table->string('position', 50)->nullable();
            $table->string('department', 50)->nullable();
            $table->date('date_hired')->nullable();
            $table->string('approved_by', 50)->nullable();
            $table->string('checked_by', 50)->nullable();
            $table->enum('status', ['PENDING', 'DISAPPROVED','APPROVED']);
            $table->enum('type_of_request',[
                'LEAVE_FORM_REQUEST',
                'CHANGE_SHIFT_REQUEST',
                'OVERTIME_REQUEST']);
            $table->text('request_information', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_request_forms');
    }
}
