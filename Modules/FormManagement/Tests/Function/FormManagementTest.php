<?php

use Tests\TestCase;
use Modules\FormManagement\Entities\FileForms;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class FormManagementTest extends TestCase
{
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function it_should_get_all_form_request()
    {
        $response = $this->json('GET', '/api/form-management/request');
        $response->assertStatus(200)
            ->assertJson([
                'message' => true,
        ]);
    }

    /**
     * @test
     */
    public function it_should_create_form_request(){
        $response = $this->json('POST', '/api/form-management/request/add', $this->createValidParams());
        $response->assertStatus(201)
            ->assertJson([
                'message' => true,
            ]);
    }

    /**
     * @test
     */
    public function it_should_update_form_request()
    {        
        $response = $this->json('PUT','/api/form-management/request/update', $this->updateValidParams()
    );
        $response->assertStatus(201);
        $response->assertJson([
            "message" => true,
        ]);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_employee_name()
    {
        $field = 'employee_name';

        $validErrorResponse = [
            "employee name is required"
        ];

        /**
         * Test if employee_name is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_employee_number()
    {
        $field = 'employee_number';

        $validErrorResponse = [
            "employee number is required"
        ];

        /**
         * Test if employee_number is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_department()
    {
        $field = 'department';

        $validErrorResponse = [
            "department is required"
        ];

        /**
         * Test if department is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_position()
    {
        $field = 'position';

        $validErrorResponse = [
            "position is required"
        ];

        /**
         * Test if position is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_type_of_request()
    {
        $field = 'type_of_request';

        $validErrorResponse = [
            "type of request is required"
        ];


        /**
         * Test if type_of_request is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_date_hired()
    {
        $field = 'date_hired';

        $validErrorResponse = [
            "date hired is required"
        ];

        /**
         * Test if date_hired is not null
         */
        $this->assertionsAdd($validErrorResponse, $field, rand(5,5));
    }
 
    public function createValidParams()
    {
        return [
            'employee_name' => rand(5,5),
            'employee_number' => 'NOC-'.rand(5,5),
            'department' => 'SI_DEV',
            'date_hired' => '2018-04-02',
            'position' => 'Back End Developer',
            'type_of_request' => config('siis.request_type.leave_form'),
            'unique_column' =>[
                'reasons_of_absence' => 'Fever',
                'inclusive_dates_from' => '2019-10-9',
                'inclusive_dates_to' => '2019-10-10',
                'number_of_days_or_hours' => '1 day',
            ]
        ];
    }

    public function updateValidParams()
    {
        $requestForm = FileForms::first();

        return [
            'id' => $requestForm->id,
            'employee_name' => rand(5,5),
            'employee_number' => 'NOC-'.rand(5,5),
            'department' => 'SI_DEV',
            'date_hired' => '2018-04-02',
            'position' => 'Back End Developer',
            'type_of_request' => config('siis.request_type.leave_form'),
            'approved_by' => 'Test1',
            'checked_by' => 'Test2',
            'status' => FileForms::REQUEST_FORM_STATUS,
        ];
    }

    public function createInvalidParam()
    {
        return [
            'employee_name' => '',
            'employee_number' => '',
            'department' => '',
            'date_hired' => '',
            'position' => '',
            'type_of_request' => '',
            'unique_column' =>[
                'reasons_of_absence' => '',
                'inclusive_dates_from' => '',
                'inclusive_dates_to' => '',
                'number_of_days_or_hours' => '',
            ]
        ];
    }

    public function assertionsAdd(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST','/api/form-management/request/add', $this->createInvalidParam($field));
        } else {
            $response = $this->json('POST','/api/form-management/request/add', $this->createInvalidParam($field, $param));
        }

        $response->assertStatus(422);

        $response->assertJson([
            "message" => true
        ]);

        $content = json_decode($response->getContent());

        $this->assertSame('The given data was invalid.', $content->message);

        foreach ($content->errors->{$field} as $error) { 
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
