<?php

namespace Modules\FormManagement\Exceptions;

use Illuminate\Http\Response;
use App\Exceptions\BaseException;
use Illuminate\Support\Facades\Log;

class FormRequestException extends BaseException
{
    private $errors;

    public function __construct($message, $errors)
    {
        $this->setMessage($message);

        $this->errors = $errors;
    }

    public function report()
    {
        $message = $this->getMessage();

        $this->setMessage(self::BAD_REQUEST_ERROR_MESSAGE);

        Log::error("[FORM_REQUEST_EXCEPTION] - {$message}");
    }

    public static function getStatusCode()
    {
        return Response::HTTP_BAD_REQUEST;
    }

    /**
     * @return array
     * @throws BaseException
     */
    public function setParams()
    {
        return [
            'message' => $this->getMessage(),
            'errors' => $this->errors,
        ];
    }
}
