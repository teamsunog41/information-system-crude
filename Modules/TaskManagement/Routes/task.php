<?php

Route::group(['prefix' => 'task'], function () {
    Route::get('', 'TaskController@getAllTask');
    Route::get('/{id}', 'TaskController@getTask');
    Route::post('', 'TaskController@createTask');
    Route::put('', 'TaskController@updateTask');
    Route::delete('/{id}', 'TaskController@deleteTask');
    Route::delete('/archive/{id}', 'TaskController@archiveTask');
    Route::patch('/assign', 'TaskController@assignTaskToUser');
    Route::patch('/start-end', 'TaskController@startOrEndTask');
    Route::patch('/close', 'TaskController@closeTask');
});
