<?php

Route::group(['prefix' => 'owner'], function () {
    Route::get('', 'OwnerController@getAllOwner');
    Route::get('/{id}', 'OwnerController@getOwner');
    Route::delete('/{id}', 'OwnerController@deleteOwner');
    Route::post('', 'OwnerController@createOwner');
    Route::put('', 'OwnerController@updateOwner');
});
