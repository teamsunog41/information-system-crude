<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\TaskManagement\Entities\Manager;

class ManagerUnitTest extends TestCase
{
    use WithoutMiddleware;
    use WithFaker;

    /**
     * @test
     */
    public function it_should_get_all_manager()
    {
        $response = $this->get('/api/task-management/manager');
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'List of Managers.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_create_manager()
    {
        $response = $this->post('/api/task-management/manager', $this->createValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Manager created successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_update_a_manager()
    {
        $response = $this->put('/api/task-management/manager', $this->updateValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Manager updated successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_get_a_manager()
    {
        $manager = Manager::inRandomOrder()->first();

        $response = $this->get('/api/task-management/manager/' . $manager->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Manager request successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_delete_a_manager()
    {
        $manager = factory(Manager::class)->create();

        $response = $this->delete('/api/task-management/manager/' . $manager->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Manager deleted successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_required_first_name()
    {
        $field = 'first_name';
        $validErrorResponse = [
            "The first name is required.",
        ];

        /**
         * Test if first_name is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_required_last_name()
    {
        $field = 'last_name';
        $validErrorResponse = [
            "The last name is required.",
        ];

        /**
         * Test if last_name is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    private function createValidParams()
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'position' => $this->faker->word(),
        ];
    }

    private function updateValidParams()
    {
        $manager = Manager::first();

        return [
            'id' => $manager->id,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'position' => $this->faker->word()
        ];
    }

    private function createInvalidParam($testField, $value = null)
    {
        $return = [
            'first_name' => '',
            'last_name' => ''
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }


    private function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST', '/api/task-management/manager', $this->createInvalidParam($field));
        } else {
            $response = $this->json('POST', '/api/task-management/manager', $this->createInvalidParam($field, $param));
        }
        
        $response->assertStatus(422)
            ->assertJson([
                "message" => true,
                "errors" => true
        ]);

        $content = json_decode($response->getContent());
        $this->assertSame('The given data was invalid.', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
