<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\TaskManagement\Entities\Project;

class ProjectUnitTest extends TestCase
{
    use WithoutMiddleware;
    use WithFaker;

    /**
     * @test
     */
    public function it_should_get_all_project()
    {
        $response = $this->get('/api/task-management/project');
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'List of Projects.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_create_a_project()
    {
        $response = $this->post('/api/task-management/project', $this->createValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Project created successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_update_a_project()
    {
        $response = $this->put('/api/task-management/project', $this->updateValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Project updated successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_get_a_project()
    {
        $project = Project::inRandomOrder()->first();

        $response = $this->get('/api/task-management/project/' . $project->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Project request successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_delete_a_project()
    {
        $project = factory(Project::class)->create();

        $response = $this->delete('/api/task-management/project/' . $project->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Project deleted successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_archive_a_project()
    {
        $project = factory(Project::class)->create();

        $response = $this->delete('/api/task-management/project/archive/' . $project->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Project archived successfully.'
            ]);

        $this->assertSoftDeleted('projects', [
            'id' => $project->id
        ]);       
    }

    /**
     * @test
     */
    public function it_should_assign_project_to_owner_or_manager()
    {
        $response = $this->patch('/api/task-management/project/assign', $this->assignValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Project assignment successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_required_name()
    {
        $field = 'name';
        $validErrorResponse = [
            "The name is required.",
        ];

        /**
         * Test if name is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_required_description()
    {
        $field = 'description';
        $validErrorResponse = [
            "The description is required.",
        ];

        /**
         * Test if description is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    private function createValidParams()
    {
        return [
            'name' => $this->faker->words(2, true),
            'description' => $this->faker->text(30),
            'owner_id' => $this->faker->randomDigitNot(0),
            'manager_id' => $this->faker->randomDigitNot(0),
            'deadline' => $this->faker->date('Y-m-d')
        ];
    }

    private function updateValidParams()
    {
        $project = Project::first();

        return [
            'id' => $project->id,
            'name' => $this->faker->words(2, true),
            'description' => $this->faker->text(30),
            'owner_id' => $this->faker->randomDigitNot(0),
            'manager_id' => $this->faker->randomDigitNot(0),
            'deadline' => $this->faker->date('Y-m-d')
        ];
    }

    private function assignValidParams()
    {
        $project = Project::first();

        return [
            'id' => $project->id,
            'owner_id' => $this->faker->randomDigitNot(0),
            'manager_id' => $this->faker->randomDigitNot(0),
        ];
    }

    private function createInvalidParam($testField, $value = null)
    {
        $return = [
            'name' => '',
            'description' => '',
            // 'owner_id' => $this->faker->word(),
            // 'manager_id' => $this->faker->word(),
            // 'deadline' => ''
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }


    private function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST', '/api/task-management/project', $this->createInvalidParam($field));
        } else {
            $response = $this->json('POST', '/api/task-management/project', $this->createInvalidParam($field, $param));
        }
        
        $response->assertStatus(422)
            ->assertJson([
                "message" => true,
                "errors" => true
        ]);

        $content = json_decode($response->getContent());
        $this->assertSame('The given data was invalid.', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
