<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\TaskManagement\Entities\Task;

class TaskUnitTest extends TestCase
{
    use WithoutMiddleware;
    use WithFaker;

     /**
     * @test
     */
    public function it_should_get_all_task()
    {
        $response = $this->get('/api/task-management/task');
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'List of Tasks.'
            ]);
    }

     /**
     * @test
     */
    public function it_should_create_a_task()
    {
        $response = $this->post('/api/task-management/task', $this->createValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Task created successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_update_a_task()
    {
        $response = $this->put('/api/task-management/task', $this->updateValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Task updated successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_get_a_task()
    {
        $task = Task::inRandomOrder()->first();

        $response = $this->get('/api/task-management/task/' . $task->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Task request successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_delete_a_task()
    {
        $task = factory(Task::class)->create();

        $response = $this->delete('/api/task-management/task/' . $task->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Task deleted successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_archive_a_task()
    {
        $task = factory(Task::class)->create();

        $response = $this->delete('/api/task-management/task/archive/' . $task->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Task archived successfully.'
            ]);

        $this->assertSoftDeleted('tasks', [
            'id' => $task->id
        ]);       
    }

    /**
     * @test
     */
    public function it_should_assign_task_to_user()
    {
        $response = $this->patch('/api/task-management/task/assign', $this->assignValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Task assigment successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_required_module_id()
    {
        $field = 'module_id';
        $validErrorResponse = [
            "The module id is required.",
        ];

        /**
         * Test if module_id is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_required_user_id()
    {
        $field = 'user_id';
        $validErrorResponse = [
            "The user id is required.",
        ];

        /**
         * Test if user_id is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_start_task()
    {
        $task = Task::first();

        $response = $this->patch('/api/task-management/task/start-end', [
            'id' => $task->id,
            'action' => 'start'
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Start or End Task successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_end_task()
    {
        $task = Task::first();

        $response = $this->patch('/api/task-management/task/start-end', [
            'id' => $task->id,
            'action' => 'end'
        ]);
        
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Start or End Task successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_close_task()
    {
        $task = Task::first();

        $response = $this->patch('/api/task-management/task/close', [
            'id' => $task->id,
            'user_id' => $this->faker->randomDigitNot(0),
            'remarks' => $this->faker->text(30)
        ]);
        
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Closing of Task successful.'
            ]);
    }

    private function createValidParams()
    {
        return [
            'module_id' => $this->faker->randomDigitNot(0),
            'user_id' => $this->faker->randomDigitNot(0),
            'subject' => $this->faker->name(),
            'description' => $this->faker->text(30),
            'priority' => $this->faker->randomElement(array_values(config('taskmanagement.priority'))),
            'status' => $this->faker->randomElement(array_values(config('taskmanagement.status'))),
            'date_assigned' => $this->faker->date('Y-m-d'),
            'deadline' => $this->faker->date('Y-m-d'),
            'created_by' => $this->faker->randomDigitNot(0),
            'assignee' => $this->faker->randomDigitNot(0),
            'start_date' => $this->faker->dateTime(),
            'end_date' => $this->faker->dateTime(),
            'closed_by' => $this->faker->randomDigitNot(0),
            'loe' => $this->faker->randomElement(array_values(config('taskmanagement.loe'))),
            'actual_hours' => $this->faker->randomDigitNot(0),
            'remarks' => $this->faker->text(50),
        ];
    }

    private function updateValidParams()
    {
        $task = Task::first();

        return [
            'id' => $task->id,
            'module_id' => $this->faker->randomDigitNot(0),
            'user_id' => $this->faker->randomDigitNot(0),
        ];
    }

    private function assignValidParams()
    {
        $task = Task::first();

        return [
            'id' => $task->id,
            'user_id' => $this->faker->randomDigitNot(0),
        ];
    }

    private function createInvalidParam($testField, $value = null)
    {
        $return = [
            'module_id' => '',
            'user_id' => '',
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }


    private function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST', '/api/task-management/task', $this->createInvalidParam($field));
        } else {
            $response = $this->json('POST', '/api/task-management/task', $this->createInvalidParam($field, $param));
        }

        $response->assertStatus(422)
            ->assertJson([
                "message" => true,
                "errors" => true
        ]);

        $content = json_decode($response->getContent());
        $this->assertSame('The given data was invalid.', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
