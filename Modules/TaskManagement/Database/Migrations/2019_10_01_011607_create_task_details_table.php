<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('task_id')->unique();
            $table->string('subject', 100)->nullable();
            $table->string('description', 250)->nullable();
            $table->enum('priority', array_values(config('taskmanagement.priority')))->default('LOW');
            $table->enum('status', array_values(config('taskmanagement.status')))->default('BACKLOG');
            $table->date('date_assigned')->nullable();
            $table->date('deadline')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('assignee')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->unsignedInteger('closed_by')->nullable();
            $table->enum('loe', array_values(config('taskmanagement.loe')))->default('1');
            $table->string('actual_hours')->nullable();
            // $table->enum('reason', array_values(config('taskmanagement.reason')))->default('CLOSE');
            $table->string('remarks')->nullable();
            // $table->foreign('task_id')->references('id')->on('tasks');
            $table->index(['subject', 'created_by']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_details');
    }
}
