<?php

namespace Modules\TaskManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TaskDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(\Modules\TaskManagement\Entities\TaskDetail::class, 20)->create([
            'task_id' => $this->getRandomTaskId()
        ]);
    }

    private function getRandomTaskId()
    {
        $task = \Modules\TaskManagement\Entities\Task::inRandomOrder()->first();
        return $task->id;
    }
}
