<?php

namespace Modules\TaskManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TaskManagementDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(OwnersTableSeeder::class);
        $this->call(ManagersTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(TaskDetailsTableSeeder::class);
        $this->call(ProjectModulesTableSeeder::class);
        $this->call(ProjectMembersTableSeeder::class);
    }
}
