<?php

namespace Modules\TaskManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProjectMembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(\Modules\TaskManagement\Entities\ProjectMember::class, 10)->create([
            'project_id' => $this->getRandomProjectId(),
            'member_id' => $this->getRandomMemberId(),
        ]);
    }

    private function getRandomProjectId()
    {
        $project = \Modules\TaskManagement\Entities\Project::inRandomOrder()->first();
        return $project->id;
    }

    private function getRandomMemberId()
    {
        $member = \Modules\AccountManagement\Entities\User::inRandomOrder()->first();
        return $member->id;
    }
}
