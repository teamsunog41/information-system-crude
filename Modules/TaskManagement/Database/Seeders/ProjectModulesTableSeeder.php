<?php

namespace Modules\TaskManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProjectModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(\Modules\TaskManagement\Entities\ProjectModule::class, 10)->create([
            'project_id' => $this->getRandomProjectId(),
            'module_id' => $this->getRandomModuleId(),
        ]);
    }

    private function getRandomProjectId()
    {
        $project = \Modules\TaskManagement\Entities\Project::inRandomOrder()->first();
        return $project->id;
    }

    private function getRandomModuleId()
    {
        $module = \Modules\TaskManagement\Entities\Module::inRandomOrder()->first();
        return $module->id;
    }
}
