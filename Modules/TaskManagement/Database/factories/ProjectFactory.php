<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(Modules\TaskManagement\Entities\Project::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true),
        'description' => $faker->text(30),
        'owner_id' => factory(Modules\TaskManagement\Entities\Owner::class)->create()->id,
        'manager_id' => factory(Modules\TaskManagement\Entities\Manager::class)->create()->id,
        'deadline' => $faker->dateTime()
    ];
});
