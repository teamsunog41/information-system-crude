<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(Modules\TaskManagement\Entities\TaskDetail::class, function (Faker $faker) {
    return [
        'task_id' => $faker->randomDigitNot(0),
        'subject' => $faker->words(2, true),
        'description' => $faker->text(30),
        'priority' => $faker->randomElement(array_values(config('taskmanagement.priority'))),
        'status' => $faker->randomElement(array_values(config('taskmanagement.status'))),
        'date_assigned' => $faker->dateTime(),
        'deadline' => $faker->dateTime(),
        'created_by' => $faker->randomDigitNot(0),
        'assignee' => $faker->randomDigitNot(0),
        'start_date' => $faker->dateTime(),
        'end_date' => $faker->dateTime(),
        'closed_by' => $faker->randomDigitNot(0),
        'loe' => $faker->randomElement(array_values(config('taskmanagement.loe'))),
        'actual_hours' => $faker->randomDigitNot(0),
        'remarks' => $faker->text(30),
    ];
});
