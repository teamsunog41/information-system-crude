<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(Modules\TaskManagement\Entities\Task::class, function (Faker $faker) {
    return [
        'module_id' => factory(Modules\TaskManagement\Entities\Module::class)->create()->id,
        'user_id' => $faker->randomDigitNot(0)
    ];
});
