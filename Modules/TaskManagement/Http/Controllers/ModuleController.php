<?php

namespace Modules\TaskManagement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\TaskManagement\Repositories\ModuleRepository;
use Modules\TaskManagement\Http\Requests\ModuleStoreRequest;

class ModuleController extends BaseController
{
    protected $moduleRepository;

    /**
     * ModuleController constructor.
     *
     * @param ModuleRepository $moduleRepository
     */
    public function __construct(ModuleRepository $moduleRepository)
    {
        $this->moduleRepository = $moduleRepository;
    }

    /**
     * Get all Module.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllModule()
    {
        $response = $this->moduleRepository->getAllModule();
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_ALL_MODULE_FAILED']);
    }

    /**
     * Create a Module.
     *
     * @param ModuleStoreRequest $moduleStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function createModule(ModuleStoreRequest $moduleStoreRequest)
    {
        $response = $this->moduleRepository->createModule($moduleStoreRequest);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['CREATE_MANAGER_FAILED']);
    }

    /**
     * Get a Module.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModule($id)
    {
        $response = $this->moduleRepository->getModule($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_OWNER_FAILED']);
    }

    /**
     * Update a Module.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateModule(Request $request)
    {
        $response = $this->moduleRepository->updateModule($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['UPDATE_OWNER_FAILED']);
    }

    /**
     * Delete a Module.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteModule($id)
    {
        $response = $this->moduleRepository->deleteModule($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['DELETE_OWNER_FAILED']);
    }

    /**
     * Assign a Module to a User.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignModuleToUser(Request $request)
    {
        $response = $this->moduleRepository->assignModuleToUser($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['MODULE_ASSIGNMENT_FAILED']);
    }
}
