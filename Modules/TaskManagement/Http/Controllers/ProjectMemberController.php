<?php

namespace Modules\TaskManagement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\TaskManagement\Repositories\ProjectMemberRepository;

class ProjectMemberController extends BaseController
{
    protected $projectMemberRepository;

    /**
     * ProjectMemberController constructor.
     *
     * @param ProjectMemberRepository $projectMemberRepository
     */
    public function __construct(ProjectMemberRepository $projectMemberRepository)
    {
        $this->projectMemberRepository = $projectMemberRepository;
    }

    /**
     * Add a Member to a Project.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMemberToProject(Request $request)
    {
        $response = $this->projectMemberRepository->addMemberToProject($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['ADD_MEMBER_TO_PROJECT_FAILED']);
    }

    /**
     * Remove a Member from a Project.
     *
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeMemberFromProject(Request $request, $project_id)
    {
        $response = $this->projectMemberRepository->removeMemberFromProject($request, $project_id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['REMOVE_MEMBER_FROM_PROJECT_FAILED']);
    }

    /**
     * Get a Project and all it's Members.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectWithMembers(Request $request)
    {
        $response = $this->projectMemberRepository->getProjectWithMembers($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_PROJECT_WITH_MEMBERS_FAILED']);
    }

    /**
     * Get a Member and all it's Projects.
     *
     * @param $member_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMemberWithProjects($member_id)
    {
        $response = $this->projectMemberRepository->getMemberWithProjects($member_id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_MEMBER_WITH_PROJECTS_FAILED']);
    }
}
