<?php

namespace Modules\TaskManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            // 'subject' => 'required|min:1',
            // 'description' => '',
            // 'assignee' => '',
            // 'level' => '',
            // 'priority' => '',
            // 'status' => '',
            // 'date_assigned' => '',
            // 'deadline' => '',
            ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => 'The :attribute is required.',
            // '*.numeric' => 'The :attribute reuires be numeric',
            // 'subject.required' => 'Task subject is required',
            // 'mobile_number' => 'Mobile number only accepts integer',
            // 'first_name.required' => 'First Name required'
        ];
    }
}
