<?php

return [
    'name' => 'TaskManagement',
    'status' => [
        'BACKLOG',
        'TO DO',
        'IN PROGRESS',
        'CODE REVIEW',
        'QA TEST',
        'UAT',
        'DONE'
    ],
    'priority' => [
        'CRITICAL',
        'HIGH',
        'LOW'
    ],
    'loe' => [
        '1',
        '2',
        '3',
        '5',
        '8',
        '13'
    ],
    'reason' => [
        'INVALID',
        'DUPLICATE',
        'CLOSE'
    ]
];
