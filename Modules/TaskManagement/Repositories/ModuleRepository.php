<?php

namespace Modules\TaskManagement\Repositories;

use App\Repositories\Repository;
use Modules\TaskManagement\Entities\Module;
use Modules\TaskManagement\Entities\ProjectModule;
use Modules\TaskManagement\Exceptions\RepositoryRequestException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ModuleRepository extends Repository
{
    const VALIDATION_MESSAGES = [
        'GET_ALL_MODULE_SUCCESS' => 'List of Modules.',
        'GET_ALL_MODULE_FAILED' => 'Failed to Get All Module.',
        'CREATE_MODULE_SUCCESS' => 'Module created successfully.',
        'CREATE_MODULE_FAILED' => 'Failed to Create Module.',
        'UPDATE_MODULE_SUCCESS' => 'Module updated successfully.',
        'UPDATE_MODULE_FAILED' => 'Failed to Update Module ID: ',
        'DELETE_MODULE_SUCCESS' => 'Module deleted successfully.',
        'DELETE_MODULE_FAILED' => 'Failed to Delete Module ID: ',
        'GET_MODULE_SUCCESS' => 'Module request successful.',
        'GET_MODULE_FAILED' => 'Failed to Get Module ID: ',
        'MODULE_ASSIGNMENT_SUCCESS' => 'Module assignment successful.',
        'MODULE_ASSIGNMENT_FAILED' => 'Failed to assign Module ID: ',
    ];

    protected $validModuleCreateAttributes = [
        'name',
        'description',
        'user_id',
        'dealine'
    ];

    protected $validProjectModuleCreateAttributes = [
        'project_id'
    ];

    public $response = [];

    public function model()
    {
        return Module::class;
    }

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    public function getAllModule()
    {
        $modules = $this->all();
        if ($modules) {
            $this->buildResponse($modules->toArray(), self::VALIDATION_MESSAGES['GET_ALL_MODULE_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_ALL_MODULE_FAILED']
        );
    }

    public function createModule($request)
    {
        DB::beginTransaction();
        try {
            $module_params = $request->only($this->validModuleCreateAttributes);
            $project_module_params = $request->only($this->validProjectModuleCreateAttributes);

            if ($module_params && $project_module_params) {
                $module = $this->model->create($module_params);
                $project_module_params['module_id'] = $module->id;
                $project_module = ProjectModule::create($project_module_params);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Create failed.',
                self::VALIDATION_MESSAGES['CREATE_MODULE_FAILED']
            );
        }
        DB::commit();

        $this->buildResponse($module->toArray(), self::VALIDATION_MESSAGES['CREATE_MODULE_SUCCESS']);
        return $this->response;
    }

    public function getModule($id)
    {
        $module = $this->find($id);
        if ($module) {
            $this->buildResponse($module->toArray(), self::VALIDATION_MESSAGES['GET_MODULE_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_MODULE_SUCCESS'] . $id . '.'
        );
    }

    public function updateModule($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $module = $this->find($request['id']);
                $module->update($request->only($this->validModuleCreateAttributes));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Update failed.',
                self::VALIDATION_MESSAGES['UPDATE_MODULE_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($module->toArray(), self::VALIDATION_MESSAGES['UPDATE_MODULE_SUCCESS']);
        return $this->response;
    }

    public function deleteModule($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $module = $this->find($id);
                $module->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Delete failed.',
                self::VALIDATION_MESSAGES['DELETE_MODULE_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($module->toArray(), self::VALIDATION_MESSAGES['DELETE_MODULE_SUCCESS']);
        return $this->response;
    }

    public function assignModuleToUser($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $module = $this->find($request['id']);
                $module->update(['user_id' => $request['user_id']]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Assignment failed.',
                self::VALIDATION_MESSAGES['MODULE_ASSIGNMENT_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($module->toArray(), self::VALIDATION_MESSAGES['MODULE_ASSIGNMENT_SUCCESS']);
        return $this->response;
    }
}
