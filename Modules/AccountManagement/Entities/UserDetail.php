<?php

namespace Modules\AccountManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'user_id',
        'photo',
        'first_name',
        'middle_name',
        'last_name',
        'nickname',
        'date_hired',
        'birth_date',
        'mobile_number',
        'level',
        'section',
        'province',
        'city',
        'town',
        'job_title',
        'team_id',
        'shift',
        'tl_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
