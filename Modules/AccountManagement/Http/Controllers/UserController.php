<?php

namespace Modules\AccountManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\BaseController;
use App\Exceptions\RepositoryRequestException;
use Modules\AccountManagement\Http\Request\UserRequest;
use Modules\AccountManagement\Repositories\UserRepository;
use Modules\AccountManagement\Repositories\UserDetailsRepository;

class UserController extends BaseController
{
    protected $userRepository;
    protected $userDetailsRepository;

    public function __construct(UserRepository $userRepository, UserDetailsRepository $userDetailsRepository)
    {
        $this->userRepository = $userRepository;
        $this->userDetailsRepository = $userDetailsRepository;
    }

    /**
     * Show the form for creating a new resource.
     * @param UserRequest $request
     * @return Response
     */
    public function create(UserRequest $request)
    {
        $user = $this->userRepository->createUser($request);
        $this->userDetailsRepository->createUserDetails($request, $user);
        return response([
            'data' => $user
        ], 200);
    }

    /**
     * Show all user.
     * @return Response
     */
    public function show()
    {
        return response([
            'data' => $this->userRepository->getAllUsers()
            ], 200);
    }

    /**
     * Show all user.
     * @param $id
     * @return Response
     * @throws RepositoryRequestException
     */
    public function showUserDetails($id)
    {
        return response([
            'data' => $this->userRepository->getUserDetails($id)
        ], 200);
    }

    /**
     * Update the specified user in db.
     * @param UserRequest $request
     * @param int $id
     * @return Response
     */
    public function updateUser(Request $request)
    {
        return response([
            'data' => $this->userRepository->updateUser($request)
        ], 200);
    }

    /**
     * Update the specified user in db.
     * @param UserRequest $request
     * @param int $id
     * @return Response
     */
    public function updateUserDetails(Request $request)
    {
        return response([
            'data' => $this->userDetailsRepository->updateUserDetails($request)
        ], 200);
    }
}
