<?php

namespace Modules\AccountManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     */
    public function login(Request $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            $tokenResult = Auth::user()->createToken('admin');

            return response()->json([
                'message' => 'Successfully logged in.',
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ], 200);
        } else {
            return response([
                'message' => 'Unauthorized: Invalid Credentials',
            ], 401);
        }
    }
}
