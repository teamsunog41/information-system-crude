<?php

namespace Modules\AccountManagement\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
|--------------------------------------------------------------------------
| Validation rule for username
|--------------------------------------------------------------------------
| May consist of lower case letter and numbers and specific special characters(._)
|
 * Class UsernameRegexRule
 * @package Modules\AccountManagement\
 */
class PasswordRegexRule implements Rule
{
    private $pattern = '/^(?=.*\d)(?=.*[@#$%^&!\*]?)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#$%^&!\*]/i';

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (preg_match($this->pattern, $value)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
