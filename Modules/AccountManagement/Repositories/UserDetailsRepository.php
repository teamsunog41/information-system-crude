<?php

namespace Modules\AccountManagement\Repositories;

use App\Exceptions\RepositoryInternalException;
use App\Exceptions\RepositoryRequestException;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Auth;
use Modules\AccountManagement\Entities\User;
use Modules\AccountManagement\Entities\UserDetail;

class UserDetailsRepository extends Repository
{
    const VALID_PARAMS = [
        'photo',
        'first_name',
        'middle_name',
        'last_name',
        'nickname',
        'date_hired',
        'birth_date',
        'mobile_number',
        'level',
        'section',
        'province',
        'city',
        'town',
        'job_title',
        'team_id',
        'shift',
        'tl_id',
    ];
    const UPDATE_VALID_PARAMS = [
        'photo',
        'first_name',
        'middle_name',
        'last_name',
        'nickname',
        'mobile_number',
        'level',
        'section',
        'province',
        'city',
        'town',
        'job_title',
        'team_id',
        'shift',
        'tl_id',
    ];

    public function model()
    {
        return UserDetail::class;
    }

    public function createUserDetails($params, $user)
    {
        $params->only(self::VALID_PARAMS);

        $userDetails = $this->model::create([
            'user_id' => $user->id,
            'photo' => $params->photo,
            'first_name' =>$params->first_name,
            'middle_name' => $params->middle_name,
            'last_name' => $params->last_name,
            'nickname' => $params->nickname,
            'mobile_number' => $params->mobile_number,
            'level' => $params->level,
            'section' => $params->section,
            'province' => $params->province,
            'city' => $params->city,
            'town' => $params->town,
            'job_title' => $params->job_title,
            'team_id' => $params->team_id,
            'shift' => $params->shift,
            'tl_id' => $params->tl_id,
        ]);

        if ($userDetails->save()) {
            $user->assignRole('employee');
            return $userDetails;
        }

        throw new RepositoryInternalException("Create user details failed!");
    }

    public function updateUserDetails($params)
    {
        $params->only(self::UPDATE_VALID_PARAMS);

        $userDetails = array (
            'photo' => $params->photo,
            'first_name' =>$params->first_name,
            'middle_name' => $params->middle_name,
            'last_name' => $params->last_name,
            'nickname' => $params->nickname,
            'mobile_number' => $params->mobile_number,
            'level' => $params->level,
            'section' => $params->section,
            'province' => $params->province,
            'city' => $params->city,
            'town' => $params->town,
            'job_title' => $params->job_title,
            'team_id' => $params->team_id,
            'shift' => $params->shift,
            'tl_id' => $params->tl_id,
        );

        $userDetailsData = $this->model::where('user_id', $params->id)->get();

        return $this->update($userDetailsData[0]->id, $userDetails);
    }
}
