<?php

namespace Modules\AccountManagement\Repositories;

use App\Exceptions\RepositoryInternalException;
use App\Exceptions\RepositoryRequestException;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Auth;
use Modules\AccountManagement\Entities\User;
use Modules\AccountManagement\Entities\UserDetail;

class UserRepository extends Repository
{
    const VALID_PARAMS = [
        'username',
        'password',
        'email',
        'updated_by',
    ];
    const UPDATE_VALID_PARAMS = [
        'password',
        'email',
        'updated_by',
        'status',
    ];

    public function model()
    {
        return User::class;
    }

    public function createUser($params)
    {
        $params->only(self::VALID_PARAMS);

        $user = $this->model::create([
            'username' => $params->username,
            'password' => bcrypt($params->password),
            'email' => $params->email,
            'created_by' => Auth::user()->id,
            'status' => 'ACTIVE',
        ]);

        if ($user->save()) {
            $user->assignRole('employee');
            return $user;
        }

        throw new RepositoryInternalException("Create user failed!");
    }

    public function updateUser($params)
    {
        $id = $params->id;

        $params->only(self::UPDATE_VALID_PARAMS);

        $user = [
            'password' => bcrypt($params->password),
            'email' => $params->email,
            'updated_by' => Auth::user()->id,
            'status' => $params->status,
        ];

        return $this->update($id, $user);
    }

    public function getAllUsers()
    {
        $user = $this->model
            ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
            ->get();

        if ($user) {
            return $user;
        }

        throw new RepositoryRequestException("No record found!");
    }

    public function getUserDetails($id)
    {
        if ($model = $this->find($id)) {
             $userDetails = $this->model
                ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                ->where('user_details.user_id', $id)
                 ->get();

            if ($userDetails) {
                return $userDetails;
            }
        }

        throw new RepositoryRequestException("Record not found!");
    }

    public function changeUserRole($id)
    {
        if ($model = $this->find($id)) {
            $userDetails = $this->model
                ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                ->where('user_details.user_id', $id)
                ->get();

            if ($userDetails) {
                return true;
            }
        }

        throw new RepositoryRequestException("Record not found!");
    }
}
