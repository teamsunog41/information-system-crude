<?php

namespace Modules\Timekeeping\Http\Controllers;

use App\Http\Controllers\BaseController;
use Modules\Timekeeping\Repositories\TimekeepingRepository;

use Illuminate\Http\Request;
use Modules\Timekeeping\Http\Requests\TimekeepingRequest;

use Illuminate\Support\Facades\File;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class TimekeepingController extends BaseController
{
    protected $timekeepingRepository;

    public function __construct(TimekeepingRepository $timekeepingRepository)
    {
        $this->timekeepingRepository = $timekeepingRepository;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return List of user with logs
     */
    public function searchUserWithLogs(Request $request)
    {
        $response = $this->timekeepingRepository->searchUserWithLogs($request->toArray());

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->timekeepingRepository::VALIDATION_MESSAGES['SYSTEM_ERROR']);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Log details
     */
    public function getDetails(Request $request)
    {
        $response = $this->timekeepingRepository->getDetails($request->toArray());

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->timekeepingRepository::VALIDATION_MESSAGES['SYSTEM_ERROR']);
    }

    /**
     * Store a newly created resource in storage.
     * @param TimekeepingRequest $timekeepingRequest
     * @return New record
     */
    public function store(TimekeepingRequest $timekeepingRequest)
    {
        $response = $this->timekeepingRepository->saveRecord($timekeepingRequest);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->timekeepingRepository::VALIDATION_MESSAGES['SYSTEM_ERROR']);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Updated record
     */
    public function update(TimekeepingRequest $timekeepingRequest)
    {
        $response = $this->timekeepingRepository->updateRecord($timekeepingRequest);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->timekeepingRepository::VALIDATION_MESSAGES['SYSTEM_ERROR']);
    }

    /**
     * Export
     * @param Request $request
     * @return download csv file
     */
    public function export(Request $request)
    {
        $content = $this->timekeepingRepository->getLogsByParams($request->toArray());
        $row = 2;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($content['data'] as $value) {
            /* EMPLOYEE INFORMATION */
            $sheet->setCellValue('A' . $row, 'Employee Name:');
            $sheet->setCellValue('B' . $row, $value['first_name'].' '.$value['middle_name'].' '.$value['last_name']);
            $row++;

            $sheet->setCellValue('A' . $row, 'Job Title:');
            $sheet->setCellValue('B' . $row, $value ['job_title']);
            $row+=2;

            /* LOGS */
            $columns = ['Date', 'Time In', 'Time Out', 'Total Log Hours', 'Total Working Hours'];
            $sheet->fromArray($columns, null, 'A' . $row);
            $row++;

            foreach ($value['logs'] as $logs) {
                $sheet->setCellValue('A' . $row, $logs['work_date']);
                $sheet->setCellValue('B' . $row, $logs['time_in']);
                $sheet->setCellValue('C' . $row, $logs['time_out']);
                $sheet->setCellValue('D' . $row, $logs['total_log_hours']);
                $sheet->setCellValue('E' . $row, $logs['total_working_hours']);
                $row++;
            }
            $row+=2;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
        $writer->setUseBOM(true);

        $date     = gmdate('Y');
        $path     = 'files/Timekeeping_' . $date;
        $filename = gmdate('m_d_Y_h_i_s') . '.csv';

        $filePath   = storage_path('app/public') . '/' . $path;
        $folderPath = storage_path() . '/app/public/' . $path . '/';

        File::makeDirectory($filePath, $mode = 0777, true, true);

        $writer->save($folderPath . $filename);
        $scheme = getenv('APP_ENV') == 'local' ? 'http://' : 'https://';
        $csvPath = $scheme . request()->getHttpHost() . '/storage/files/Timekeeping_' . $date . '/' .  $filename;

        return $csvPath;
    }
}
