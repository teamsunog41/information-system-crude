<?php

namespace Modules\Timekeeping\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The module namespace to assume when generating URLs to actions.
     *
     * @var string
     */
    protected $moduleNamespace = 'Modules\Timekeeping\Http\Controllers';

    /**
     * Called before routes are registered.
     *
     * Register any model bindings or pattern based filters.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        $this->mapApiRoutes($router);
    }


    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @param Router $router
     */
    protected function mapApiRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->moduleNamespace,
            'middleware' => 'api'
        ], function ($router) {
            $router->group([
                'prefix' => 'api/timekeeping/'
            ], function ($router) {
                foreach (glob(base_path('Modules/Timekeeping/Routes/*.php')) as $route_file) {
                    include $route_file;
                }
            });
        });
    }
}
