<?php

use Tests\TestCase;
use Modules\Timekeeping\Entities\Timekeeping;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TimekeepingFunctionTest extends TestCase
{
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function it_should_create_log()
    {
        $response = $this->json('POST', '/api/timekeeping/add', $this->createValidParams());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_update_log()
    {
        $response = $this->json('PUT','/api/timekeeping/update' , $this->updateValidParams());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_search_logs()
    {
        $response = $this->json('POST', '/api/timekeeping/search', $this->createValidSearchParams());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_search_logs_by_id()
    {
        $response = $this->json('POST', '/api/timekeeping/search', $this->createValidSearchParamsById());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_search_logs_by_level()
    {
        $response = $this->json('POST', '/api/timekeeping/search', $this->createValidSearchParamsByLevel());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_search_logs_by_job_title()
    {
        $response = $this->json('POST', '/api/timekeeping/search', $this->createValidSearchParamsByJobTitle());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_search_logs_by_keyword()
    {
        $response = $this->json('POST', '/api/timekeeping/search', $this->createValidSearchParamsByKeyword());
        $response->assertStatus(200)
                 ->assertJson(['message' => true]);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_user_id()
    {
        $field = 'user_id';

        $validErrorResponse = [
            "The user id field is required."
        ];

        /**
         * Test if user_id is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_work_date()
    {
        $field = 'work_date';

        $validErrorResponse = [
            "The work date field is required."
        ];

        /**
         * Test if work_date is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_time_in()
    {
        $field = 'time_in';

        $validErrorResponse = [
            "The time in field is required."
        ];

        /**
         * Test if time_in is not null
         */
        $this->assertionsAdd($validErrorResponse, $field);
    }
 
    public function createValidParams()
    {
        return [
            'user_id' => '1', 
            'work_date' => date('Y-m-d'), 
            'time_in' => '08:55:10', 
            'time_out' => null
        ];
    }

    public function updateValidParams()
    {
        $logDetails = Timekeeping::first();

        return [ 
            'id' => $logDetails->id,
            'time_out' => '19:05:00'
        ];
    }

    public function createValidSearchParams()
    {
        return [
            'user_id' => '',
            'level' => 'Mid',
            'job_title' => 'Developer',
            'keyword' => 'Mel'
        ];
    }

    public function createValidSearchParamsById()
    {
        return [
            'user_id' => '1',
            'level' => '',
            'job_title' => '',
            'keyword' => ''
        ];
    }

    public function createValidSearchParamsByLevel()
    {
        return [
            'user_id' => '',
            'level' => 'Mid',
            'job_title' => '', 
            'keyword' => ''
        ];
    }

    public function createValidSearchParamsByJobTitle()
    {
        return [
            'user_id' => '',
            'level' => '',
            'job_title' => 'Developer', 
            'keyword' => ''
        ];
    }

    public function createValidSearchParamsByKeyword()
    {
        return [
            'user_id' => '',
            'level' => '',
            'job_title' => '', 
            'keyword' => 'Gilbert'
        ];
    }

    public function createInvalidParams()
    {
        return [
            'user_id' => '', 
            'work_date' => '', 
            'time_in' => '', 
            'time_out' => ''
        ];
    }

    public function assertionsAdd(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST','/api/timekeeping/add', $this->createInvalidParams($field));
        } else {
            $response = $this->json('POST','/api/timekeeping/add', $this->createInvalidParams($field, $param));
        }

        $response->assertStatus(422);

        $response->assertJson([
            "message"=>true
        ]);

        $content = json_decode($response->getContent());

        $this->assertSame('Invalid form values!', $content->message);

        foreach ($content->errors->{$field} as $error) { 
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
