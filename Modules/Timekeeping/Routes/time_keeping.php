<?php

Route::post('search', 'TimekeepingController@searchUserWithLogs')->name('time-keeping.searchUserWithLogs');
Route::post('details', 'TimekeepingController@getDetails')->name('time-keeping.getDetails');
Route::post('add', 'TimekeepingController@store')->name('time-keeping.store');
Route::put('update', 'TimekeepingController@update')->name('time-keeping.update');
Route::post('export', 'TimekeepingController@export')->name('time-keeping.export');