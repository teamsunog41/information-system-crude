<?php
return [
    'job_titles' => [
        'Developer',
        'Quality Assurance',
        'Systems Analyst',
    ],

    'shifts' => [
        'Regular Shift' => '9am - 7pm',
        'Mid Shift' => '2pm - 12pm',
        'Night Shift' => '9pm - 7pm',
    ],

    'status' => ['ACTIVE', 'DISABLED', 'TERMINATED'],
];
